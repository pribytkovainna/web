﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace WebApplication2
{
    public class Startup1
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(20);
                options.Cookie.Name = ".MyWebApp";
            });
            services.AddSignalR();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSession();

            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("chat");
            });

            app.UseMiddleware<SessionAuthenticationMiddleware>();

            app.Map("/ajax", HandleAjax);

            app.Map("/signin", HandleSignIn);

            app.Map("/logout", HandleLogout);

            app.Map("/login", HandleLogin);

            app.Map("/upload", HandleUpload);

            app.Map("/files", HandleFiles);

            app.Map("/chatting", HandleChat);

            app.Use(async (context, next) =>
            {
                var path = context.Request.Path.ToString();
                path = System.Net.WebUtility.UrlDecode(path);

                if (File.Exists("Files" + path))
                {
                    var bytes = File.ReadAllBytes("Files/" + path);
                    context.Response.ContentLength = bytes.Length;
                    await context.Response.Body.WriteAsync(bytes, 0, bytes.Length);
                }
                await next.Invoke();
            });

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync(File.ReadAllText("error.html"));
            });
        }

        private void HandleChat(IApplicationBuilder builder)
        {
            builder.Run(async context =>
                await context.Response.WriteAsync(File.ReadAllText("chat.html"))
            );
        }

        private void HandleAjax(IApplicationBuilder builder)
        {
            builder.Run(async context =>
            {
                if (context.Request.Method == "GET" && context.Request.ContentType == "application/xml")
                {
                    await context.Response.WriteAsync(DateTime.Now.ToString());
                }
                else
                {
                    context.Response.Clear();
                }
            });
        }

        private void HandleSignIn(IApplicationBuilder builder)
        {
            builder.Run(async context =>
            {
                if (context.Request.Method == "GET")
                {
                    await context.Response.WriteAsync(File.ReadAllText("signin.html"));
                }
                else
                {
                    var login = context.Request.Form["login"];
                    var email = context.Request.Form["email"];
                    var password = context.Request.Form["password"];

                    using (var data = new UsersContext())
                    {
                        if (!data.Users.Any(user => user.Login == login))
                        {
                            data.Users.Add(new User
                            {
                                Email = email,
                                Login = login,
                                Password = password
                            });
                            data.SaveChanges();
                        }
                    }
                    context.Response.Redirect("/");
                }
            });
        }

        private void HandleLogout(IApplicationBuilder builder)
        {
            builder.Run(async context =>
            {
                context.Session.Remove("name");
                context.Response.Redirect("/login");
            });
        }

        private void HandleLogin(IApplicationBuilder builder)
        {
            builder.Run(async context =>
            {
                if (context.Request.Method == "GET")
                {
                    var session = context.Session;
                    if (session.Keys.Contains("name"))
                    {
                        context.Response.Redirect("/");
                    }

                    await context.Response.WriteAsync(File.ReadAllText("login.html"));
                }
                else if (context.Request.Method == "POST")
                {
                    var login = context.Request.Form["login"];
                    var password = context.Request.Form["password"];

                    User user = null;
                    using (var data = new UsersContext())
                    {
                        user = data
                        .Users
                        .FirstOrDefault(x => x.Login == login && x.Password == password);

                        if (user != null)
                        {
                            var userString = JsonConvert.SerializeObject(user);
                            context.Session.SetString("name", userString);
                        }
                    }
                    context.Response.Redirect("/");
                }
            });
        }

        private void HandleFiles(IApplicationBuilder builder)
        {
            builder.Run(async context =>
            {
                StringValues fileExtensions = context.Request.Query["extension"];

                var page = File.ReadAllText("template.html");

                var sb = new StringBuilder();
                foreach (string file in Directory.GetFiles("Files/"))
                {
                    var filename = file.Replace("Files/", "");
                    var extension = Path.GetExtension(filename).Substring(1);
                    if (fileExtensions.Count == 0 || fileExtensions.Contains(extension))
                        sb.AppendLine($"<a href=\"{filename}\">{filename}</a><p/>");
                }

                page = page.Replace("<!body/>", sb.ToString());

                await context.Response.WriteAsync(page);
            });
        }

        private void HandleUpload(IApplicationBuilder app)
        {
            app.Map("/file", HandleUploadFile);

            app.Map("/text", HandleCountWords);

            app.Run(Handle);

            async System.Threading.Tasks.Task Handle(HttpContext context)
            {

                await context.Response.WriteAsync(File.ReadAllText("upload.html"));
            }
        }

        private void HandleUploadFile(IApplicationBuilder builder)
        {
            builder.Run(async context =>
            {
                if (context.Request.Method == "POST" && context.Request.HasFormContentType)
                {
                    var form = context.Request.Form;
                    foreach (var file in form.Files)
                    {
                        using (var stream = new FileStream("Files/" + file.FileName, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                    }
                    context.Response.Redirect("/upload/files");
                }
                await context.Response.WriteAsync(File.ReadAllText("uploadfile.html"));
            });
        }

        private void HandleCountWords(IApplicationBuilder builder)
        {
            builder.Run(async context =>
            {
                if (context.Request.Method == "POST")
                {
                    string text;
                    using (var reader = new System.IO.StreamReader(context.Request.Body))
                    {
                        text = reader.ReadToEnd();
                    }
                    text = text.Replace("text=", "");
                    int wordsCount = 0;
                    if (!string.IsNullOrWhiteSpace(text))
                        wordsCount = text.Split('+', StringSplitOptions.RemoveEmptyEntries).Length;
                    string str = wordsCount.ToString();
                    await context.Response.WriteAsync(
                        @"<!DOCTYPE html><html><head><meta charset=""utf-8"" /></head><body>" + str + "</body></html>"
                        );
                }
                else
                {
                    await context.Response.WriteAsync(File.ReadAllText("uploadtext.html"));
                }
            });
        }
    }
}