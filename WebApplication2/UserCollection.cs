﻿using System.Collections.Generic;

namespace WebApplication2
{
    public class UserCollection
    {
        private static Dictionary<string, User> _users = new Dictionary<string, User>()
        {
            {"Admin", new User(){Email="admin@gmail.com",Login="admin",Password="admin"}}
        };
        public static void Add(User user)
        {
            _users.Add(user.Login, user);

        }
         
        public static User Get(string login)
        {
            _users.TryGetValue(login, out var user);
            return user;
        }
    }
}
