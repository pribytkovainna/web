﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2
{
    public class SessionAuthenticationMiddleware
    {
            private RequestDelegate _next;

            public SessionAuthenticationMiddleware(RequestDelegate next)
            {
                _next = next;
            }
            public async Task Invoke(HttpContext context)
            {
                string[] whiteList = { "/login", "/signin" };
                if (whiteList.Contains(context.Request.Path.Value) || context.Session.Keys.Contains("name"))
                    _next.Invoke(context);
                else
                    context.Response.Redirect("/login");
            }
        
    }
}